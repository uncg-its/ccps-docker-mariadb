# Cloud Collaboration & Productivity Services, ITS, University of North Carolina at Greensboro

#########################
#						#
#   USAGE				#
#						#
#########################

# Use like any regular *nix Makefile.
# syntax: make <command>
# Example: make build-image

#########################
#						#
#   DOCKER SPECIFICS	#
#						#
#########################

view-instance-logs:
	docker logs instance-mariadb

get-instance-ip:
	docker inspect instance-mariadb | grep "IPAddress"

build-image-with-proxy:
	docker build  --build-arg http_proxy=http://proxy.uncg.edu:3128 --build-arg https_proxy=http://proxy.uncg.edu:3128 --build-arg HTTP_PROXY=http://proxy.uncg.edu:3128 --build-arg HTTPS_PROXY=http://proxy.uncg.edu:3128 -t img-mariadb .

build-image-no-proxy:
	docker build -t img-mariadb .

rebuild-image-no-proxy:
	docker build --pull --no-cache -t img-mariadb .

rebuild-image-with-proxy:
	docker build --pull --no-cache   --build-arg http_proxy=http://proxy.uncg.edu:3128 --build-arg https_proxy=http://proxy.uncg.edu:3128 --build-arg HTTP_PROXY=http://proxy.uncg.edu:3128 --build-arg HTTPS_PROXY=http://proxy.uncg.edu:3128 -t img-mariadb .

remove-image:
	docker rmi img-mariadb

#required for SELINUX machines to allow writing to the databases directory on the host
db-folder-writeable-for-selinux:
	chcon -Rt svirt_sandbox_file_t databases

#required for SELINUX machines to allow writing to the databases directory on the host
# change based on ITS Systems Recommendation on 2018-05-02
db-folder-writable-for-selinux-new:
	semanage fcontext -a -t container_file_t "databases/.*"
	restorecon -R databases

create-network:
	docker network create --driver bridge ccps_docker_network

# NOTE: instance may need to be restarted again if the local folders do not exist already before the first run.
run-instance:
	docker run \
	-p 3306:3306 \
	--name instance-mariadb \
	-v $(CURDIR)/databases:/var/lib/mysql \
	-v $(CURDIR)/mounted-volumes/conf/mysql:/etc/mysql/conf.d/:Z \
	-e MYSQL_ALLOW_EMPTY_PASSWORD=yes \
	--restart always \
	--network=ccps_docker_network \
	-d img-mariadb
	docker restart instance-mariadb


restart-instance:
	docker restart instance-mariadb

stop-instance:
	docker stop instance-mariadb

stop-remove-instance:
	docker stop instance-mariadb
	docker rm instance-mariadb

login-to-instance:
	docker exec -it instance-mariadb bash

#########################
#						#
#   MAINTENANCE SETS	#
#						#
#########################

script-rebuild-tear-down-restart-no-proxy:
	make rebuild-image-no-proxy
	make stop-remove-instance
	make run-instance

script-rebuild-tear-down-restart-with-proxy:
	make rebuild-image-with-proxy
	make stop-remove-instance
	make run-instance

prune-images:
	docker image prune --force

#########################
#						#
#   UNCG HELPERS		#
#						#
#########################

export-uncg-proxy:
	export https_proxy="proxy.uncg.edu:3128" && export http_proxy="proxy.uncg.edu:3128" && export HTTP_PROXY="proxy.uncg.edu:3128" && export HTTPS_PROXY="proxy.uncg.edu:3128"
