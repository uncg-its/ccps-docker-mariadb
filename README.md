# Welcome
Repository for the CCPS group's work on Docker containers for MariaDB.

The intent is to use this container to replace the packaged versions of MariaDB available in RHEL7.

This should also allow the CCPS group to maintain consistency across local/dev/val/prod environments by keeping version numbers in sync.


# Getting Started
(Standard Makefile usage is followed. View /Makefile for all available options)
```sh
$ more Makefile
```

## MYSQL Conf
Create your configuration file for MySQL - it will be added to the `/etc/mysql/conf.d` folder, where it will be loaded my MySQL at runtime. You can make a new file or use the existing `conf-examples/ccps.cnf` as a base.
```sh
$ cp conf-examples/ccps.cnf mounted-volumes/conf/mysql/ccps.cnf
```

> Note that binary logging is disabled by default as of version 1.5.0. If you want to re-enable it, you should add the requisite options to your custom `.cnf` file.

# Build the Image

First, build the image:
```sh
$ make build-image-no-proxy // for local development
OR
$ make build-image-with-proxy // for remote deployment behind proxy
```

FOR SELINUX deployments
```sh
$ make db-folder-writeable-for-selinux
and/or
$ make db-folder-writeable-for-selinux-new
```

Create the Docker Network for this instance to run on

Note: you only need to do this if the network isn't already created (ie: if you're also using the PHP/Apache CCPS instance)
```sh
$ make create-network
```

IMPORTANT:The next step will create your MariaDB instance with no root password. You MUST (especially in production) run the secure-installation steps below to set a root password etc.

Create/run an instance based on the image we built above:
```sh
$ make run-instance
```

Login to the instance to configure MySQL/MariaDB. Do this either by creating a new admin that can login remotely to add new databases and users OR create a scoped user for each DB/user pairing
```sh
$ make login-to-instance
```
This will put you inside the container, ready for you to configure MariaDB
```sh
$ mysql_secure_installation
```
Follow prompts on screen choosing to set a root password (write it down somewhere), and remove test databases etc
```sh
$ mysql -u root -p // password is whatever you set above during mysql_secure_installation
```
(Optional) Create an admin user who can remotely create databases and users (helpful if administering from SequelPro etc on desktop machine)
```sh
$ CREATE USER 'myNewRemoteUser'@'%' IDENTIFIED BY '5up3r5tr0ngPa$sw0rd';
$ GRANT ALL PRIVILEGES ON *.* TO 'myNewRemoteUser'@'%' WITH GRANT OPTION;
```
Exit MySQL and Instance to get back to the host machine's shell prompt.
```sh
$ exit // closes mysql prompt
$ exit // closes instance prompt
```
Next, try to connect to your new DB instance (ie: from SequelPro):
```sh
Host: 127.0.0.1
Username: {myNewRemoteUser} // from above
Password: {5up3r5tr0ngPa$sw0rd} // from above
```

Next create a database + user to host an app on
```sh
\\ either do this via command line (after make login-to-instance) or remotely using the account you created in the step above
```

That's it.

# Other Stuff
* databases are kept in /databases/ directory and are persistent across instance restarts/rebuilds.
* If not working behind a proxy, comment out the lines at the top of Dockerfile

# Tagged Version Details

## 1.7.0

- FROM: mariadb:10.6

## 1.6.0
- FROM: mariadb:10.5

## 1.5.0
- Disable binary logging by default to save disk space. Can be re-added in the mounted `ccps.cnf` file with `log-bin` and `binlog-format=MIXED`. These flags were passed in at runtime via Makefile in previous versions.

## 1.4.0
- Explicitly calling `rebuild` during the automated teardown / rebuild scripts, and ensuring that the `--pull` flag is used so that latest version is used

## 1.3.1
- `.dockerignore` added for smaller build contexts

## 1.3.0
- FROM mariadb:10.4
- replaces outdated `MAINTAINER` directive
- fixes an issue with `make run-instance` where the incorrect image name was being used

## 1.2.0
- FROM mariadb:10.4.1
- Adds config mapping

## 1.1
- FROM mariadb:10.3.1
- Added rebuild script process to makefile

## 1.0
- FROM mariadb:10.3.1
